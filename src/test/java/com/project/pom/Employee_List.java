package com.project.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Employee_List extends Base{
	
	By LoginLocator = By.id("btnLogin");
	By MenuLocator = By.linkText("PIM");
	By seeList = By.linkText("Employee List");
	By filter = By.id("employee_name_quick_filter_employee_list_value");
	By filterSearch = By.id("quick_search_icon");
	
	public Employee_List(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public void see_Employee() throws InterruptedException {
		click(LoginLocator);
		Thread.sleep(5000);
		click(MenuLocator);
		Thread.sleep(5000);
		click(seeList);
		Thread.sleep(15000);
		type("Pablo Parada", filter);
		Thread.sleep(5000);
		click(filterSearch);
		Thread.sleep(5000);
	}

}
