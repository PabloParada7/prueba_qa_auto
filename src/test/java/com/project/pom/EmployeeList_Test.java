package com.project.pom;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class EmployeeList_Test {
	
	private WebDriver driver;
	
	Employee_List employee_list;

	@Before
	public void setUp() throws Exception {
		employee_list = new Employee_List(driver);
		driver = employee_list.chromeDriverConnection();
		employee_list.visit("https://orangehrm-demo-6x.orangehrmlive.com/");
		driver.manage().window().maximize();
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void test() throws InterruptedException {
		employee_list.see_Employee();
		Thread.sleep(5000);
		
	}

}
