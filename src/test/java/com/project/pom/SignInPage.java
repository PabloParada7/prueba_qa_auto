package com.project.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SignInPage extends Base {
	
	By LoginLocator = By.id("btnLogin");
	By MenuLocator = By.linkText("PIM");
	By AddEmployeeLocator = By.linkText("Add Employee");
	By FirstNameLocator = By.id("firstName");
	By SecondNameLocator = By.id("middleName");
	By LastNameLocator = By.id("lastName");
	By dropList = By.xpath("//*[@id=\"location_inputfileddiv\"]/div/input");
	By option = By.cssSelector("ul[style]>li[class='']>span");
	By BtnSave = By.id("systemUserSaveBtn");// boton next primera pag
	By Date = By.name("dateTextInput");
	By MaritalStatus = By.xpath("//*[@id=\"emp_marital_status_inputfileddiv\"]/div/input");
	By MaritalStatusSelect = By.cssSelector("ul[style]> li:nth-child(2) > span");
	By Genero = By.xpath("//*[@id=\"emp_gender_inputfileddiv\"]/div/input");
	By GeneroSelect = By.cssSelector("ul[style]>li:nth-child(4)>span");
	By Nationality = By.xpath("//*[@id=\"nation_code_inputfileddiv\"]/div/input");
	By NationalitySelect = By.cssSelector("ul[style]>li:nth-child(40)>span");
	By NickName = By.id("nickName");
	By Check = By.xpath("//*[@id=\"pimPersonalDetailsForm\"]/materializecss-decorator[5]/div/sf-decorator[3]/div/label");
	By Blood = By.xpath("//*[@id=\"1_inputfileddiv\"]/div/input");
	By BloodSelect = By.cssSelector("ul[style]> li:nth-child(5)>span");
	By Hobbie = By.id("5");
	By BtnSave2 = By.xpath("//*[@id=\"wizard-nav-button-section\"]/button[2]");
	By Job = By.name("dateTextInput");
	By Region = By.xpath("//*[@id=\"9_inputfileddiv\"]/div/input");
	By RegionSelect = By.xpath("//li/span[contains(text(),'Region-3')]");
	By Fte = By.xpath("//*[@id=\"10_inputfileddiv\"]/div/input");
	By FteSelect = By.xpath("//li/span[contains(text(),'0.75')]");
	By Depar = By.xpath("//*[@id=\"11_inputfileddiv\"]/div/input");
	By DeparSelect = By.xpath("//li/span[contains(text(),'Sub unit-4')]");
	By BtnSave3 = By.xpath("//*[@id=\"wizard-nav-button-section\"]/button[3]");
	
	public SignInPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public void agregar() throws InterruptedException {
		click(LoginLocator);
		Thread.sleep(3000);
		click(MenuLocator);
		Thread.sleep(5000);
		click(AddEmployeeLocator);
		Thread.sleep(15000);
		type("Pablo", FirstNameLocator);
		Thread.sleep(3000);
		type("Andres", SecondNameLocator);
		Thread.sleep(3000);
		type("Parada", LastNameLocator);
		Thread.sleep(3000);
		click(dropList);
		Thread.sleep(3000);
		click(option);
		Thread.sleep(3000);
		click(BtnSave);
		Thread.sleep(20000);
		type("Mon, 11 Jul 1994", Date);
		Thread.sleep(3000);
		click(MaritalStatus);
		Thread.sleep(3000);
		click(MaritalStatusSelect);
		Thread.sleep(3000);
		click(Genero);
		Thread.sleep(3000);
		click(GeneroSelect);
		Thread.sleep(3000);
		click(Nationality);
		Thread.sleep(3000);
		click(NationalitySelect);
		Thread.sleep(3000);
		type("Pablete", NickName);
		Thread.sleep(3000);
		click(Check);
		Thread.sleep(3000);
		click(Blood);
		Thread.sleep(3000);
		click(BloodSelect);
		Thread.sleep(3000);
		type("Playstation", Hobbie);
		Thread.sleep(3000);
		click(BtnSave2);
		Thread.sleep(20000);
		type("Mon, 20 Jul 2020", Job);
		Thread.sleep(3000);
		click(Region);
		Thread.sleep(3000);
		click(RegionSelect);
		Thread.sleep(3000);
		click(Fte);
		Thread.sleep(3000);
		click(FteSelect);
		Thread.sleep(3000);
		click(Depar);
		Thread.sleep(3000);
		click(DeparSelect);
		Thread.sleep(3000);
		click(BtnSave3);
		Thread.sleep(20000);
	}

}
